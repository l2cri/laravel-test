<?php

namespace App;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Song extends Eloquent
{
	/**
	* Fillable fields for a song
	*@var array
	*/
    protected $fillable = [
    	'title','lyrics','slug','user_id'
    ];

    /**
     * An songs is owned by user
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    
    /**
     * Get tags with associated songs
     * 
     * @return type
     */
    public function tags() 
    {
        return $this->belongsToMany('App\Tag')->withTimestamps();
    }
}
