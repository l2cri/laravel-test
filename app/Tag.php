<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    /**
     * Get the song associated with the given tag
     * 
     * @return type
     */
    public function songs() {
        return $this->belongsToMany('App\Song');
    }
    
}
