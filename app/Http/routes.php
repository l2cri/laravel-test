<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

$router->bind('songs',function($slug){
    return App\Song::where('slug',$slug)->first();
});

Route::get('/','PagesController@index');

$router->get('contact', ['as' => 'contact_page', 'uses' => 'PagesController@contact']);

Route::get('about',['middleware'=> 'demo','uses'=>'PagesController@about']);


$router->resource("songs","SongsPagesController",[
    //'except' => 'create',
    'names' => [
    	'index' => 'songs_path',
    	'show' => 'song_path'
    ]
]);

Route::controllers([
    'auth' => 'Auth\AuthController',
    'password' => 'Auth\PasswordController'
]);

$router->resource("peoples","PeopleController");

Route::get('foo',['middleware'=> 'manager',  function(){
        return 'Эта страница видна только менеджерам';
}]);