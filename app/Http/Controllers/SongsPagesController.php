<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Song;

use App\Http\Requests;
use App\Http\Requests\CreateSongRequest;
use App\Http\Controllers\Controller;

class SongsPagesController extends Controller
{
    private $song;
    private $user;


    public function __construct(Song $song) {
        
        $this->middleware('auth',['only' => 'create']);
        
        $this->song = $song;
        $this->user = \Auth::user();
    }


    /**
    * List of songs
    */
    public function index() {
        $songs = $this->song->get();
        //dd($songs);die();
        
        return view('songs.index',  compact('songs'));
    }
    
    /**
    * Show individual song
    */
    public function show(Song $song) {
        
        //$song =  Song::whereSlug($slug)->first();
        //dd($song);die();
        
        return view('songs.show',  compact('song'));
    }
    
    /**
    * Show edit form
    */
    public function edit(Song $song) {
        return view('songs.edit',  compact('song'));
    }
    
    /**
    * Record update song in DB
    */
    public function update(Request $request,Song $song){
        
        //dd($request->get('title'));

        // $song->title = $request->get('title');
        // $song->save();
        $song->fill($request->input())->save();



        return redirect('songs');
    }

    /**
    * Show form to add new song
    */
    public function create (){
        
        /*if(\Auth::guest()){
            
            return redirect('songs');
            
        }*/
        
        return view('songs.create');
    }

    /**
    * Save new song
    */
    public function store(CreateSongRequest $request) {

        $input = $request->all();
        //dd($input);

        $song = new Song($input);

        $this->user->songs()->save($song);
        
        //session()->flash('flash_message','Your song has been created!');
        //session()->flash('flash_message_important',true);
        
        flash()->overlay('Ваша песня была сохранена', 'Хорошая работа!');

        return redirect()->route('songs_path');
    }

    public function destroy(Song $song) {
        $song->delete();

        return redirect()->route('songs_path');
    }
    
}
