<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

Admin::model(\App\Song::class)
        ->title('Songs List')
        ->as('songs-alias')
        ->columns(function ()
        {
            // create columns for this model here
            Column::string('title', 'Title');
            Column::string('lyrics', 'Lyrics');
            Column::string('slug', 'Slug');
        })
        ->form(function(){
            FormItem::text('title', 'Title');
            FormItem::textarea('lyrics', 'Lyrics');
            FormItem::text('slug', 'Slug');
        });