<!DOCTYPE html>
<html>
    <head>
        <title>Title</title>
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    </head>
    <body>
        <div class="container">
            @include('flash::message')
            
            @yield('content')
        </div>
        
        <script src="//code.jquery.com/jquery.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
        
        <script>
            $('#flash-overlay-modal').modal();
            //$('div.alert').not('.alert-important').delay(3000).slideUp(300);
        </script>
        @yield('footer')
        <p><a href="{{ route('contact_page') }}">Contact me</a></p>
    </body>
</html>