@extends('app')

@section('content')
    {!! Form::open(['url'=> 'auth/login']) !!}
        <div class="form-group">
            {!! Form::label('email',"Email") !!}
            {!! Form::email('email',old('email'),['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('password',"Password") !!}
            {!! Form::password('password',['class' => 'form-control']) !!}
        </div>

        <div class="form-group">
            {!! Form::label('remember',"Remember Me") !!}
            {!! Form::checkbox('remember') !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Login',['class' => 'btn btn-primary']) !!}
        </div>
    {!! Form::close() !!}
@stop