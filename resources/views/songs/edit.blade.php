@extends('app')

@section('content')
    <h1>Song {{ $song->id }}</h1>
    
    {{ $song->title }}
    
    {!! Form::model($song, ['route'=> ['songs.update',$song->slug],'method'=>'PATCH']) !!}
        
        @include('songs.form')
       
    {!! Form::close() !!}

    {!! delete_form(['songs.destroy',$song->slug],'Удалить') !!}
@stop