@extends('app')

@section('content')
    <h1>Song {{ $song->id }}</h1>
    
    <h2>{{ $song->title }}</h2>

    @if ($song->lyrics)
	    <article class="lyrics">
	 		{!! nl2br($song->lyrics) !!}
	    </article>
	@endif

	<div>User_ID : {{ $song->user_id }}</div>


	{!! link_to_route('songs.edit','Изменить', [$song->slug]) !!}<br>
	{!! link_to_route('songs_path','Вернуться в список песен') !!}

@stop