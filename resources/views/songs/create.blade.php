@extends('app')

@section('content')
    <h1>Добавить новую песню</h1>
    
    {!! Form::open(['route'=> 'songs.store']) !!}
        
        @include('songs.form')

    {!! Form::close() !!}
@stop