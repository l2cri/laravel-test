<!--Temporary -->

<div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
    {!! Form::label('title',"Заголовок") !!}
    {!! Form::text('title',null,['class' => 'form-control']) !!}
    {!! $errors->first('title', '<span class="help-block">:message</span>') !!}
</div>

<div class="form-group {{ $errors->has('lyrics') ? 'has-error' : '' }}">
    {!! Form::label('lyrics',"Текст песни") !!}
    {!! Form::textarea('lyrics',null,['class' => 'form-control']) !!}
    {!! $errors->first('lyrics', '<span class="help-block">:message</span>') !!}
</div>
<div class="form-group {{ $errors->has('slug') ? 'has-error' : '' }}">
    {!! Form::label('slug',"ЧПУ") !!}
    {!! Form::text('slug',null,['class' => 'form-control']) !!}
    {!! $errors->first('slug', '<span class="help-block">:message</span>') !!}
</div>
<div class="form-group">
    {!! Form::submit('Обновить песню',['class' => 'btn btn-primary']) !!}
</div>